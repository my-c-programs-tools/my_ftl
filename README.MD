# My FTL
My FTL is a command line C implementation of the famous video game Faster Than Light.
The aim of this game is to cross the galaxy you're in by jumping from stellar system to stellar system. You might have some troubles by meeting enemies.
This project was carried out in November 2016 during my fourth year of programming studies.

## USAGE
Build from source and execute
```
$> make
$> ./my_ftl
```

## HOW TO PLAY

You have to cross the galaxy passing through 10 different stellar systems. In every stellar system, you can use DETECT command to find for freight. When you find a fret, you can use GETBONUS command to improve your ship stats.
When an enemie has destroyed part of your ship, you must repair it using de REPAIRSYSTEM command and provide which part should be repaired. To know exactly which part is damaged, you can use the command CONTROLSYSTEM.
At any time, you can use the HELP command as a reminder of all available commands.

Have fun :)