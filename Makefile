##
## Makefile for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
## 
## Made by VEROVE Jordan
## Login   <verove_j@etna-alternance.net>
## 
## Started on  Mon Nov  7 11:53:12 2016 VEROVE Jordan
## Last update Wed Oct 16 10:13:00 2019 VEROVE Jordan
##

SRC_DIR =	srcs/

CC =		gcc

CFLAGS =	-Wall -Werror -Wextra

RM =		rm -rf

SRC =		$(SRC_DIR)print_tools.c \
		$(SRC_DIR)print_state_help.c \
		$(SRC_DIR)print_win_loose.c \
		$(SRC_DIR)string_tools.c \
		$(SRC_DIR)air_shed.c \
		$(SRC_DIR)container.c \
		$(SRC_DIR)system_control.c \
		$(SRC_DIR)system_repair.c \
		$(SRC_DIR)read_line.c \
		$(SRC_DIR)game_loop.c \
		$(SRC_DIR)action.c \
		$(SRC_DIR)enemy.c \
		$(SRC_DIR)my_put_nbr.c \
		$(SRC_DIR)check.c \
		$(SRC_DIR)main.c

OBJ =		$(SRC:.c=.o)

NAME =		my_FTL

$(NAME):	$(OBJ)
		$(CC) $(CFLAGS) $(OBJ) -o $(NAME)

all:		$(NAME)

clean:
		$(RM) $(OBJ)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all

.PHONY:		all clean fclean re
