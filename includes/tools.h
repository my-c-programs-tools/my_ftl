/*
** tools.h for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Mon Nov  7 12:54:18 2016 VEROVE Jordan
** Last update Thu Nov 10 15:02:45 2016 VEROVE Jordan
*/

#ifndef _TOOLS_H_
# define _TOOLS_H_

#include <stdlib.h>
#include "ftl.h"

extern const char	*reset_color;

typedef struct		s_color
{
  char			*color;
  char			*unicode;
}			t_color;

static const t_color	g_color[] =
  {
    {"clear", "\033[H\033[2J"},
    {"red", "\033[31m"},
    {"green", "\033[32m"},
    {"yellow", "\033[33m"},
    {"blue", "\033[34m"},
    {"magenta", "\033[35m"},
    {"cyan", "\033[36m"},
    {NULL, NULL}
  };

void			my_putchar(char c);
void			my_putstr(const char *str);
void			my_putstr_color(const char *color, const char *str);
void			my_print_prompt();
void			my_print_usr_state(t_ship *ship);
void			my_print_enemy_state(t_ship *enemy);
void			my_print_help();
void			my_print_win();
void			my_print_loose();
int			    my_strlen(const char *str);
int			    my_strcmp(const char *s1, const char *s2);
char            to_lower(char c);
char			*my_strdup(const char *src);
char			*my_strcpy(char *dest, char *src);
char			*read_line();
void			my_put_nbr(int nb);

#endif /* !_TOOLS_H_ */
