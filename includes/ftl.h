/*
** ftl.h for my_FTL in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Mon Nov  7 11:40:29 2016 VEROVE Jordan
** Last update Fri Nov 11 11:11:25 2016 VEROVE Jordan
*/

#ifndef _FTL_H_
# define _FTL_H_

#define TRUE 1
#define FALSE 0

typedef struct		s_freight
{
  char			*item;
  struct s_freight	*prev;
  struct s_freight	*next;
}			t_freight;

typedef struct		s_container
{
  t_freight		*first;
  t_freight		*last;
  int			nb_elem;
}			t_container;

typedef struct		s_navigation_tools
{
  char			*system_state;
  int			sector;
  int			dodge;
}			t_navigation_tools;

typedef struct		s_ftl_drive
{
  char			*system_state;
  int			energy;
}			t_ftl_drive;

typedef struct		s_weapon
{
  char			*system_state;
  int			damage;
}			t_weapon;

typedef struct		s_ship
{
  int			hull;
  t_weapon		*weapon;
  t_ftl_drive		*drive;
  t_navigation_tools	*nav_tools;
  t_container		*container;
}			t_ship;

typedef struct		s_repair_command
{
  char			*tools_to_repair;
  int			(*repair_fct)(t_ship *ship);
}			t_repair_command;

typedef struct		s_check_ret
{
  int			ret_detect;
  int			ret_att_jmp;
  int			ret_repair;
}			t_check_ret;

typedef struct		s_check_bool
{
  int			detect;
  int			is_alive;
}			t_check_bool;

t_ship			*create_ship(int print, int life);
int			add_weapon_to_ship(t_ship *ship, int damage,
					   int print);
int			add_ftl_drive_to_ship(t_ship *ship);
int			add_navigation_tools_to_ship(t_ship *ship);
int			add_container_to_ship(t_ship *ship);
void			add_freight_to_container(t_ship *ship,
						 t_freight *freight);
void			del_freight_from_container(t_ship *ship,
					   t_freight *freight);
void			get_bonus(t_ship *ship);
void			ftl_drive_system_check(t_ship *ship);
void			navigation_tools_system_check(t_ship *ship);
void			weapon_system_check(t_ship *ship);
void			system_control(t_ship *ship);
int			ftl_drive_system_repair(t_ship *ship);
int			navigation_tools_system_repair(t_ship *ship);
int			weapon_system_repair(t_ship *ship);
int			system_repair(t_ship *ship);
int			game_loop(t_ship *ship);
void			get_usr_input(t_ship *ship, t_ship *enemy,
				      t_check_bool *check,
				      t_check_ret *ret);
int			check_detect_bonus(char *usr_input, t_ship *ship,
					   int *check_detect);
int			check_attack_jump(t_ship *ship, t_ship *enemy,
					  char *usr_input);
int			check_repair_stat_help_ctrl(t_ship *ship,
						     char *usr_input);
int			check_destroy(t_ship *ship, t_ship *enemy);
int			check_is_alive(t_ship *ship, int *is_alive);
void			recall_get_usr_input(t_ship *ship, t_ship *enemy,
					     t_check_bool *check_bool,
					     t_check_ret *ret);
int			detect_containers(t_ship *ship, int *detect);
int			jump_sector(t_ship *ship, t_ship *enemy);
int			attack(t_ship *ship, t_ship *enemy);
t_ship			*gen_random_enemy();
int			enemy_attack(t_ship *enemy, t_ship *ship);
void			random_destroy_system(t_ship *ship);
void			check_to_destroy(t_ship *ship, int random_system,
					 int random_destroy);

#endif /* !_FTL_H_ */
