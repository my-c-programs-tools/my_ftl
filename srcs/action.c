/*
1;4002;0c** action.c for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Wed Nov  9 11:20:03 2016 VEROVE Jordan
** Last update Wed Oct 16 10:18:40 2019 VEROVE Jordan
*/

#include "../includes/ftl.h"
#include "../includes/tools.h"

t_freight	*gen_random_freight()
{
  t_freight	*new_freight;
  int		rand_value;
  
  if ((new_freight = malloc(sizeof(t_freight))) == NULL)
    return (NULL);
  rand_value = (rand() % 100) + 1;
  if (rand_value <= 70)
    new_freight->item = my_strdup("scrap");
  else
    {
      rand_value = (rand() % 100) + 1;
      if (rand_value <= 25)
	new_freight->item = my_strdup("energy");
      else if (rand_value > 25 && rand_value <= 50)
	new_freight->item = my_strdup("attackbonus");
      else if (rand_value > 50 && rand_value <= 75)
	new_freight->item = my_strdup("lifepoints");
      else if (rand_value > 75 && rand_value <= 99)
	new_freight->item = my_strdup("dodgebonus");
      else
	new_freight->item = my_strdup("scrap");
    }
  return (new_freight);
}

int		detect_containers(t_ship *ship, int *detect)
{
  t_freight	*new_freight;
  int		nb;

  nb = 0;
  if (my_strcmp(ship->nav_tools->system_state, "online") != 0)
    {
      my_putstr_color("red", "[DETECTION SYSTEM]: Unable to scan this stellar system because the navigation tools are out of order!\n");
      return (1);
    }
  else
    {
      *detect = TRUE;
      my_putstr_color("yellow", "[DETECTION SYSTEM]: Freight has been detected inside the stellar system\nFreight recrovery...\n");
      my_putstr_color("green", "[DETECTION SYSTEM]: Freight added!\n");
      while (nb != 10)
	{
	  if ((new_freight = gen_random_freight()) == NULL)
	    return (-1);
	  add_freight_to_container(ship, new_freight);
	  nb++;
	}
    }
  return (1);
}

int		jump_sector(t_ship *ship, t_ship *enemy)
{
  if (enemy != NULL)
    {
      my_putstr_color("red", "You cannot leave this stellar system because an enemy is facing you! (he can move faster than you and he get more energy, run is not an option!)\n");
      return (1);
    }
  else if (my_strcmp(ship->drive->system_state, "online") == 0)
    {
      ship->drive->energy -= 1;
      return (2);
    }
  else if (my_strcmp(ship->drive->system_state, "online") != 0)
    {
      my_putstr_color("red", "[SYSTEM FAILURE]: You cannot leave this stellar system, your reactor is out of order\n");
      return (1);
    }
  return (1);
}

int		attack(t_ship *ship, t_ship *enemy)
{
  if (ship == NULL || enemy == NULL)
    {
      my_putstr_color("red", "No enemy found in this stellar system!\n");
      return (1);
    }
  else if (ship != NULL && enemy != NULL)
    {
      if (my_strcmp(ship->weapon->system_state, "online") == 0)
	{
	  enemy->hull -= ship->weapon->damage;
	  return (check_destroy(ship, enemy));
	}
      else
	{
	  my_putstr_color("red", "Your ship's weapon system is out of order, you must repair it to attack\n");
	  return (1);
	}
    }
  return (0);
}
