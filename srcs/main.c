/*
** main.c for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Mon Nov  7 11:51:37 2016 VEROVE Jordan
** Last update Wed Oct 16 10:20:00 2019 VEROVE Jordan
*/

#include <stdlib.h>
#include <time.h>

#include "../includes/ftl.h"
#include "../includes/tools.h"

void		print_start()
{
  my_putstr_color("clear", "");
  my_putstr_color("cyan", " _______  _   _       _______ _______\n\
 |  |  |   \\_/        |____      |    |\n |  |  |    |         |  \
        |    |_____\n\n");
}

t_ship		*init_player_ship()
{
  t_ship	*new_ship;

  if ((new_ship = create_ship(TRUE, 50)) == NULL)
    return (NULL);
  if (add_weapon_to_ship(new_ship, 10, TRUE) == 0)
    return (NULL);
  if (add_ftl_drive_to_ship(new_ship) == 0)
    return (NULL);
  if (add_navigation_tools_to_ship(new_ship) == 0)
    return (NULL);
  if (add_container_to_ship(new_ship) == 0)
    return (NULL);
  return (new_ship);
}

void		my_free_ship(t_ship *ship)
{
  free(ship->weapon->system_state);
  free(ship->weapon);
  free(ship->drive->system_state);
  free(ship->drive);
  free(ship->nav_tools->system_state);
  free(ship->nav_tools);
  free(ship->container);
  free(ship);
}

int		main()
{
  t_ship	*new_ship;

  srand(time(NULL));
  print_start();
  if ((new_ship = init_player_ship()) == NULL)
    return (1);
  if (game_loop(new_ship) == 2)
    my_print_loose(new_ship);
  else
    my_print_win();
  my_free_ship(new_ship);
  return (0);
}
