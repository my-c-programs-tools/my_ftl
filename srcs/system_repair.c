/*
** system_repair.c for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Tue Nov  8 10:33:38 2016 VEROVE Jordan
** Last update Sun May  3 20:57:59 2020 VEROVE Jordan
*/

#include "../includes/ftl.h"
#include "../includes/tools.h"

const t_repair_command repair_tab[] = {
  {"drive system", &ftl_drive_system_repair},
  {"navigation system", &navigation_tools_system_repair},
  {"weapon system", &weapon_system_repair},
  {NULL, NULL}};

int		ftl_drive_system_repair(t_ship *ship)
{
  my_putstr_color("yellow", "Reactor repair in progress...\n");
  free(ship->drive->system_state);
  if ((ship->drive->system_state = my_strdup("online")) == NULL)
    {
      my_putstr_color("red", "Reactor repair has failed\n");
      return (0);
    }
  else
    my_putstr_color("green", "Repair complete! Reactor system is OK\n");
  return (1);
}

int		navigation_tools_system_repair(t_ship *ship)
{
  my_putstr_color("yellow", "Navigation system repair in progress...\n");
  free(ship->nav_tools->system_state);
  if ((ship->nav_tools->system_state = my_strdup("online")) == NULL)
    {
      my_putstr_color("red", "Navigation system repair has failed\n");
      return (0);
    }
  else
    my_putstr_color("green", "Repair complete! Navigation system is OK\n");
  return (1);
}

int		weapon_system_repair(t_ship *ship)
{
  my_putstr_color("yellow", "Weapon system repair in progress...\n");
  free(ship->weapon->system_state);
  if ((ship->weapon->system_state = my_strdup("online")) == NULL)
    {
      my_putstr_color("red", "Weapon system repair has failed\n");
      return (0);
    }
  else
    my_putstr_color("green", "Repair complete! Weapon system is OK\n");
  return (1);
}

int		system_repair(t_ship *ship)
{
  char		*user_input;
  int		i;
  int		check_user_input;

  i = 0;
  check_user_input = FALSE;
  my_putstr_color("cyan", "System to repair~>");
  if ((user_input = read_line()) == NULL)
    my_putstr_color("red", "[SYSTEM FAILURE]: the command reader is broken\n");
  while (repair_tab[i].tools_to_repair != NULL && check_user_input == FALSE)
    {
      if (my_strcmp(repair_tab[i].tools_to_repair, user_input) == 0)
	{
	  if (repair_tab[i].repair_fct(ship) == 0)
	    return (0);
	  check_user_input = TRUE;
	}
      else
	i++;
    }
  if (repair_tab[i].tools_to_repair == NULL)
    my_putstr_color("red", "[SYSTEM FAILURE]: unknown repair command\n");
  return (1);
}
