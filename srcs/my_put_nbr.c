/*
** my_put_nbr.c for libmy in /home/verove-j/EtnaRendu/libmy/tmp
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Sat Oct 22 15:21:08 2016 VEROVE Jordan
** Last update Wed Oct 16 10:17:23 2019 VEROVE Jordan
*/

#include "../includes/tools.h"

void	my_put_tmp_nbr(int nb, int check)
{
  if (nb < 0 && check == 0)
    {
      my_putchar('-');
    }
  if (nb >= 10 || nb <= -10)
    {
      my_put_tmp_nbr(nb / 10, 1);
      my_put_tmp_nbr(nb % 10, 1);
    }
  else if (nb >= 0)
    my_putchar(nb + 48);
  else if (nb < 0)
    my_putchar((-nb * 2) + nb + 48);
}

void	my_put_nbr(int nb)
{
  my_put_tmp_nbr(nb, 0);
}
