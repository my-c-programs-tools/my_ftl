/*
** system_control.c for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Tue Nov  8 10:09:21 2016 VEROVE Jordan
** Last update Wed Oct 16 10:19:10 2019 VEROVE Jordan
*/

#include "../includes/ftl.h"
#include "../includes/tools.h"

void		ftl_drive_system_check(t_ship *ship)
{
  my_putstr_color("yellow", "Checking reactor...\n");
  if (my_strcmp(ship->drive->system_state, "online") == 0)
    my_putstr_color("green", "Reactor is OK!\n");
  else
    my_putstr_color("red", "Reactor is out!\n");
}

void		navigation_tools_system_check(t_ship *ship)
{
  my_putstr_color("yellow", "Checking navigation system...\n");
  if (my_strcmp(ship->nav_tools->system_state, "online") == 0)
    my_putstr_color("green", "Navigation system is OK!\n");
  else
    my_putstr_color("red", "Navigation system is out!\n");
}

void		weapon_system_check(t_ship *ship)
{
  my_putstr_color("yellow", "Checking weapon system...\n");
  if (my_strcmp(ship->weapon->system_state, "online") == 0)
    my_putstr_color("green", "Weapon system is OK!\n");
  else
    my_putstr_color("red", "Weapon system is out!\n");
}

void		system_control(t_ship *ship)
{
  ftl_drive_system_check(ship);
  weapon_system_check(ship);
  navigation_tools_system_check(ship);
}
