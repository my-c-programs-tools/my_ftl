/*
** print_state.c for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Wed Nov  9 18:49:20 2016 VEROVE Jordan
** Last update Wed Oct 16 10:16:11 2019 VEROVE Jordan
*/

#include "../includes/ftl.h"
#include "../includes/tools.h"

void			my_print_usr_state(t_ship *ship)
{
  my_putstr_color("cyan", "[STATE OF YOUR SHIP]:\n   |   Life points:");
  my_put_nbr(ship->hull);
  my_putstr_color("cyan", "\n   |   Damages:");
  my_put_nbr(ship->weapon->damage);
  my_putstr_color("cyan", "\n   |   Energy:");
  my_put_nbr(ship->drive->energy);
  my_putstr_color("cyan", "\n   |   Sector:");
  my_put_nbr(ship->nav_tools->sector);
  my_putstr_color("cyan", "\n   |   Dodge:");
  my_put_nbr(ship->nav_tools->dodge);
  my_putchar('\n');
}

void			my_print_enemy_state(t_ship *enemy)
{
  my_putstr_color("cyan", "[STATE OF THE ENEMY' SHIP]:\n   |   Life points:");
  my_put_nbr(enemy->hull);
  my_putstr_color("cyan", "\n   |   Damages:");
  my_put_nbr(enemy->weapon->damage);
  my_putchar('\n');
}

void			my_print_help()
{
  my_putstr_color("yellow", "Available system commands :\n");
  my_putstr("attack\ndetect\njump\ngetbonus\nsystem control\nsystem repair\n\
   navigation system\n   weapon system\n   drive system\nstat\nhelp\n");
}
