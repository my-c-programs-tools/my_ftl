/*
** air_shed.c for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Mon Nov  7 11:44:34 2016 VEROVE Jordan
** Last update Wed Oct 16 10:16:46 2019 VEROVE Jordan
*/

#include <stdlib.h>

#include "../includes/ftl.h"
#include "../includes/tools.h"

const char	*reset_color;

int			add_navigation_tools_to_ship(t_ship *ship)
{
  t_navigation_tools	*new_tools;

  my_putstr_color("yellow", "Adding navigation tools...\n");
  if ((new_tools = malloc(sizeof(t_navigation_tools))) == NULL)
    {
      my_putstr_color("red", "Your ship has been destroyed when you tried to had some navigation tools\n");
      return (0);
    }
  if ((new_tools->system_state = my_strdup("online")) == NULL)
    return (0);
  new_tools->sector = 0;
  new_tools->dodge = 25;
  if (ship)
    ship->nav_tools = new_tools;
  else
    {
      my_putstr_color("red", "The ship does not exist\n");
      return (0);
    }
  my_putstr_color("green", "The navigation tool has been successfully added to your ship!\n");
  return (1);
}

int			add_ftl_drive_to_ship(t_ship *ship)
{
  t_ftl_drive		*new_drive;

  my_putstr_color("yellow", "Adding reactor...\n");
  if ((new_drive = malloc(sizeof(t_ftl_drive))) == NULL)
    {
      my_putstr_color("red", "Your ship has been destroyed when you tried to add the reactor to your ship\n");
      return (0);
    }
  if ((new_drive->system_state = my_strdup("online")) == NULL)
    return (0);
  new_drive->energy = 10;
  if (ship)
    ship->drive = new_drive;
  else
    {
      my_putstr_color("red", "The ship does not exist\n");
      return (0);
    }
  my_putstr_color("green", "The reactor has been successfully added to your ship!\n");
  return (1);
}

int			add_weapon_to_ship(t_ship *ship, int damage, int print)
{
  t_weapon		*new_weapon;

  if (print == TRUE)
    my_putstr_color("yellow", "Adding weapons...\n");
  if ((new_weapon = malloc(sizeof(t_weapon))) == NULL)
    {
      if (print == TRUE)
	my_putstr_color("red", "Your ship has been destroyed when you tried\
 to add the weapons to your ship\n");
      return (0);
    }
  if ((new_weapon->system_state = my_strdup("online")) == NULL)
    return (0);
  new_weapon->damage = damage;
  if (ship)
    ship->weapon = new_weapon;
  else
    {
      if (print == TRUE)
	my_putstr_color("red", "The ship does not exist\n");
      return (0);
    }
  if (print == TRUE)
    my_putstr_color("green", "The weapons has been successfully added to your ship!\n");
  return (1);
}

t_ship			*create_ship(int print, int life)
{
  t_ship		*new_ship;

  if (print == TRUE)
    my_putstr_color("green", "Building your ship...\n");
  if ((new_ship = malloc(sizeof(t_ship))) == NULL)
    {
      if (print == TRUE)
	my_putstr_color("red", "We lack materials to build your ship !\n");
      return (NULL);
    }
  new_ship->hull = life;
  new_ship->weapon = NULL;
  new_ship->drive = NULL;
  new_ship->container = NULL;
  if (print == TRUE)
    my_putstr_color("green", "The ship improvement is complete! You are ready to explore the M87 galaxy :D (Warning of his supermassive blackhole in the galaxy center)\n");
  return (new_ship);
}
