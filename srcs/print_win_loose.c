/*
** my_print_win_loose.c for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Thu Nov 10 14:59:23 2016 VEROVE Jordan
** Last update Wed Oct 16 10:18:18 2019 VEROVE Jordan
*/

#include <sys/types.h>
#include <sys/stat.h>
//#include <fcntl.h>

#include "../includes/ftl.h"
#include "../includes/tools.h"

void		my_print_win()
{
  my_putstr_color("green", "Well played !! You crossed this galaxy so much faster than light!!! \n");
}

void		my_print_loose(t_ship *ship)
{
  if (ship->hull <= 0)
    my_putstr_color("red", "Your ship has been destroyed by enemy...\n");
  else if (ship->drive->energy <= 0)
    my_putstr_color("red", "Your ship has no energy anymore. It will only suffer the gravity from the nearest objects from now. Hope that the closest object is not a blockhole ;)\n");
  my_putstr_color("red", "  ______ _______ _______ _______       _____  _    _ \
_______  ______\n |  ____ |_____| |  |  | |______      |     |  \\  /  |______ \
|_____/\n |_____| |     | |  |  | |______      |_____|   \\/   |______ |    \\_\
\n\n");
}
