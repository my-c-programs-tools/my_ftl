/*
1;4002;0c** game_loop.c for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Wed Nov  9 09:38:31 2016 VEROVE Jordan
** Last update Wed Oct 16 10:17:13 2019 VEROVE Jordan
*/

#include "../includes/ftl.h"
#include "../includes/tools.h"

void		reset_fct_ret(t_check_ret *ret)
{
  ret->ret_detect = 0;
  ret->ret_att_jmp = 0;
  ret->ret_repair = 0;
}

void		recall_get_usr_input(t_ship *ship, t_ship *enemy,
				     t_check_bool *check_bool, t_check_ret *fct_ret)
{
  if (fct_ret->ret_detect == 0 && fct_ret->ret_att_jmp == 0 &&
      fct_ret->ret_repair == 0)
    my_putstr_color("red", "[SYSTEM FAILURE]: Unknown command\n");  
  get_usr_input(ship, enemy, check_bool, fct_ret);
}

void		get_usr_input(t_ship *ship, t_ship *enemy, t_check_bool *check,
			      t_check_ret *fct_ret)
{
  char		*usr_input;
  int		enemy_attack_ret;

  enemy_attack_ret = 0;
  reset_fct_ret(fct_ret);
  if (enemy != NULL)
    enemy_attack_ret = enemy_attack(enemy, ship);
  if (!(enemy_attack_ret == 2))
    {
      my_print_prompt();
      if ((usr_input = read_line()) == NULL)
	my_putstr_color("red", "[SYSTEM FAILURE]:Ooops! Seems that the command reader is broken\n");
      if ((fct_ret->ret_detect =
	   check_detect_bonus(usr_input, ship, &(check->detect))) == 0)
	if ((fct_ret->ret_att_jmp = check_attack_jump(ship, enemy, usr_input)) == 0)
	  fct_ret->ret_repair = check_repair_stat_help_ctrl(ship, usr_input);
      free(usr_input);
      if (fct_ret->ret_att_jmp == 3)
	enemy = NULL;
      if (fct_ret->ret_att_jmp != 2 && check_is_alive(ship, &(check->is_alive))
	  == TRUE)
	recall_get_usr_input(ship, enemy, check, fct_ret);
    }
  else
    check->is_alive = FALSE;
}

int		game_loop(t_ship *ship)
{
  t_ship	*enemy;
  t_check_bool	check;
  t_check_ret	fct_ret;
 
  check.is_alive = TRUE;
  while (ship->nav_tools->sector != 10)
    {
      check.detect = FALSE;
      enemy = gen_random_enemy();
      get_usr_input(ship, enemy, &check, &fct_ret);
      if (enemy != NULL)
	{
	  free(enemy->weapon->system_state);
	  free(enemy->weapon);
	  free(enemy);
	}
      if (check.is_alive == FALSE)
	return (2);
      ship->nav_tools->sector += 1;
    }
  return (1);
}
