/*
** enemy.c for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Wed Nov  9 14:09:55 2016 VEROVE Jordan
** Last update Wed Oct 16 10:19:25 2019 VEROVE Jordan
*/

#include <stdlib.h>

#include "../includes/ftl.h"
#include "../includes/tools.h"

void		check_to_destroy(t_ship *ship, int random_system,
				 int random_destroy)
{
  if (random_destroy <= 0)
    {
      if (random_system == 0 &&
	  my_strcmp(ship->weapon->system_state, "online") == 0)
	{
	  ship->weapon->system_state = my_strcpy(ship->weapon->system_state,
						 "noline");
	  my_putstr_color("red", "[CRITICAL] Ship's weapons has been destroyed\n");
	}
      else if (random_system == 1 &&
	  my_strcmp(ship->nav_tools->system_state, "online") == 0)
    	{
	  ship->nav_tools->system_state =
	    my_strcpy(ship->nav_tools->system_state, "noline");
	  my_putstr_color("red", "[CRITICAL] Ship's navigation system has been destroyed\n");
	}
      else if (random_system == 2 &&
	  my_strcmp(ship->drive->system_state, "online") == 0)
	{
	  ship->drive->system_state = my_strcpy(ship->drive->system_state,
						 "noline");
	  my_putstr_color("red", "[CRITICAL] Ship's reactor has been destroyed\n");
	}
    }
}

void		random_destroy_system(t_ship *ship)
{
  int		random_system;
  int		random_destroy;

  random_destroy = (rand() % 100) - 19;
  random_system = (rand() % 2);
  check_to_destroy(ship, random_system, random_destroy);
}

int		enemy_attack(t_ship *enemy, t_ship *ship)
{
  int		rand_avoid;

  rand_avoid = (rand() % 100) - ship->nav_tools->dodge;
  if (rand_avoid > 0)
    {
      ship->hull -= enemy->weapon->damage;
      my_putstr_color("yellow", "The enemy has touched you\n");
      random_destroy_system(ship);
      if (ship->hull <= 0)
	return (2);
    }
  else
    {
      my_putstr_color("green", "The enemy missed his attack\n");
    }
  return (1);
}

t_ship		*gen_random_enemy()
{
  int		rand_value;
  static int	enemy_lp = 20;
  static int	enemy_dmg = 10;
  t_ship	*enemy;

  rand_value = (rand() % 100) - 29;
  if (rand_value <= 0)
    {
      if ((enemy = create_ship(FALSE, enemy_lp)) == NULL)
	return (NULL);
      if (add_weapon_to_ship(enemy, enemy_dmg, FALSE) == 0)
	return (NULL);
      my_print_enemy_state(enemy);
      enemy_lp += enemy_lp / 2;
      enemy_dmg += enemy_dmg / 2;
      return (enemy);
    }
  return (NULL);
}
