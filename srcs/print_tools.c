/*
** tools.c for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Mon Nov  7 11:55:53 2016 VEROVE Jordan
** Last update Wed Oct 16 10:15:51 2019 VEROVE Jordan
*/

#include <unistd.h>
#include "../includes/tools.h"

const char		*reset_color = "\033[0m";

void			my_putchar(char c)
{
  write(1, &c, 1);
}

void			my_putstr(const char *str)
{
  write(1, str, my_strlen(str));
}

void			my_putstr_color(const char *color, const char *str)
{
  int			i;

  i = 0;
  while (g_color[i].color != NULL && (my_strcmp(g_color[i].color, color) != 0))
    i++;
  if (g_color[i].color == NULL)
    {
      my_putstr(str);
      return ;
    }
  my_putstr(g_color[i].unicode);
  my_putstr(str);
  my_putstr(reset_color);
}

void			my_print_prompt()
{
  my_putstr_color("blue", "Waiting for orders~>");
}
