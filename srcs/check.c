/*
** check.c for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Wed Nov  9 18:41:02 2016 VEROVE Jordan
** Last update Wed Oct 16 10:19:40 2019 VEROVE Jordan
*/

#include "../includes/ftl.h"
#include "../includes/tools.h"

int		check_destroy(t_ship *ship, t_ship *enemy)
{
  if (enemy->hull <= 0)
    {
      my_putstr_color("green", "You killed the enemy!\n");
      if ((rand() % 1) == 1)
	ship->drive->energy -= 1;
      return (3);
    }
  else
    {
      my_print_enemy_state(enemy);
      return (1);
    }
  return (0);
}

int		check_is_alive(t_ship *ship, int *is_alive)
{
  if (ship->hull <= 0 || ship->drive->energy <= 0)
    {
      *is_alive = FALSE;
      return (FALSE);
    }
  return (TRUE);
}

int		check_detect_bonus(char *usr_input, t_ship *ship,
				   int *check_detect)
{
  int		detect_value;

  if (my_strcmp("detect", usr_input) == 0)
    {
      if (*check_detect == FALSE)
	{
	  if ((detect_value = detect_containers(ship, check_detect)) == 1)
	    return (1);
	  else if (detect_value == -1)
	    return (-1);
	}
      else
	{
	  my_putstr_color("red", "only one detect per sector is allowed!!!\n");
	  return (1);
	}
      return (0);
    }
  else if (my_strcmp("getbonus", usr_input) == 0)
    {
      get_bonus(ship);
      return (1);
    }
  return (0);
}

int		check_attack_jump(t_ship *ship, t_ship *enemy, char *usr_input)
{
  if (my_strcmp("attack", usr_input) == 0)
    return (attack(ship, enemy));
  else if (my_strcmp("jump", usr_input) == 0)
    return (jump_sector(ship, enemy));
  return (0);
}

int		check_repair_stat_help_ctrl(t_ship *ship, char *usr_input)
{
  if (my_strcmp("stat", usr_input) == 0)
    {
      my_print_usr_state(ship);
      return (1);
    }  
  else if (my_strcmp("system repair", usr_input) == 0)
    return (system_repair(ship));
  else if (my_strcmp("help", usr_input) == 0)
    {
      my_print_help();
      return (1);
    }
  else if (my_strcmp("system control", usr_input) == 0)
    {
      system_control(ship);
      return (1);
    }
  return (0);
}
