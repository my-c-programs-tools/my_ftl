/*
** string_tools.c for my_ftl in /home/jordan/Downloads
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Mon Nov  7 13:22:38 2016 VEROVE Jordan
** Last update Wed Oct 16 10:18:29 2019 VEROVE Jordan
*/

#include "../includes/tools.h"

char			*my_strcpy(char *dest, char *src)
{
  int			i;

  i = 0;
  while (src[i] != 0)
    {
      dest[i] = src[i];
      i++;
    }
  dest[i] = 0;
  return (dest);
}

char        to_lower(char c)
{
    if (c <= 90 && c >= 65)
        return (c + 32);
    return (c);
}

int			my_strcmp(const char *s1, const char *s2)
{
  int			i;

  if (s1 == NULL || s2 == NULL)
    return (-2);
  i = 0;
  while (s1[i] != '\0')
    {
      if (to_lower(s1[i]) > to_lower(s2[i]))
	return (1);
      else if (to_lower(s1[i]) < to_lower(s2[i]))
	return (-1);
      i++;
    }
  if (s2[i] != '\0')
    return (-1);
  return (0);
}

char			*my_strdup(const char *str)
{
  int			i;
  char			*copy;
  
  i = 0;
  copy = NULL;
  if ((copy = malloc((my_strlen(str) + 1) * sizeof(char))) == NULL)
    return (NULL);
  while (str[i] != '\0')
    {
      copy[i] = str[i];
      i++;
    }
  copy[i] = '\0';
  return (copy);
}

int			my_strlen(const char *str)
{
  int			i;

  i = 0;
  while (str[i] != 0)
    i++;
  return (i);
}
