/*
** container.c for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Mon Nov  7 16:33:08 2016 VEROVE Jordan
** Last update Wed Oct 16 10:17:02 2019 VEROVE Jordan
*/

#include "../includes/ftl.h"
#include "../includes/tools.h"

void		get_bonus(t_ship *ship)
{
  t_freight	*tmp;

  tmp = ship->container->first;
  if (tmp) {
    my_putstr_color("yellow", "Taking useful materials...\n");
    my_putstr_color("green", "Useful materials recovered\n");
  }
  else {
    my_putstr_color("red", "No freight available\n");
  }
  while (tmp != NULL)
    {
      if (my_strcmp(tmp->item, "attackbonus") == 0)
	{
	  my_putstr_color("green", "damage +5\n");
	  ship->weapon->damage += 5;
	}
      else if (my_strcmp(tmp->item, "dodgebonus") == 0)
	{
	  my_putstr_color("green", "dodge +3\n");
	  ship->nav_tools->dodge += 3;
	}
      else if (my_strcmp(tmp->item, "energy") == 0)
	{
	  my_putstr_color("green", "energy +1\n");
	  ship->drive->energy += 1;
	}
      else if (my_strcmp(tmp->item, "lifepoints") == 0)
	{
	  my_putstr_color("green", "lifepoints +5\n");
	  ship->hull += 5;
	}
      del_freight_from_container(ship, tmp);
      tmp = tmp->next;
    }
}

void		del_freight_from_container(t_ship *ship, t_freight *freight)
{
  t_freight	*tmp;
  t_freight	*prev;
  
  tmp = ship->container->first;
  while (tmp != NULL && tmp != freight)
    tmp = tmp->next;
  if (tmp == freight)
    {
      if (tmp == ship->container->first && ship->container->first->next != NULL)
	{
	  ship->container->first = ship->container->first->next;
	  ship->container->first->prev = NULL;
	}
      else if (tmp == ship->container->first &&
	       ship->container->first->next == NULL)
	ship->container->first = NULL;
      else
	{
	  prev = tmp->prev;
	  prev->next = tmp->next;
	}
      ship->container->nb_elem -= 1;
      free(tmp->item);
      free(tmp);
    }
}

void		add_freight_to_container(t_ship *ship, t_freight *freight)
{
  t_freight	*tmp;

  if (ship->container->first == NULL)
    {
      ship->container->first = freight;
      ship->container->first->next = NULL;
      ship->container->first->prev = NULL;
      ship->container->nb_elem += 1;
    }
  else
    {
      tmp = ship->container->first;
      ship->container->first = freight;
      ship->container->first->next = tmp;
      ship->container->first->prev = tmp->prev;
      tmp->prev = ship->container->first;
      ship->container->nb_elem += 1;
    }
}

int		add_container_to_ship(t_ship *ship)
{
  t_container	*new_container;

  my_putstr_color("yellow", "Adding container...\n");
  if ((new_container = malloc(sizeof(t_container))) == NULL)
    {
      my_putstr_color("red", "Your ship has been destroyed when you tried to get the container...\n");
      return (0);
    }
  new_container->first = NULL;
  new_container->last = NULL;
  new_container->nb_elem = 0;
  if (ship)
    ship->container = new_container;
  else
    {
      my_putstr_color("red", "The ship does not exist\n");
      return (0);
    }
  my_putstr_color("green", "The container has been successfully added!\n");
  return (1);
}
