/*
** read_line.c for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Tue Nov  8 11:19:08 2016 VEROVE Jordan
** Last update Tue Nov  8 11:20:59 2016 VEROVE Jordan
*/

#include	<stdlib.h>
#include	<unistd.h>

char		*read_line(void)
{
  ssize_t	ret;
  char		*buff;

  if ((buff = malloc((50 + 1) * sizeof(char))) == NULL)
    return (NULL);
  if ((ret = read(0, buff, 50)) > 1)
    {
      buff[ret - 1] = '\0';
      return (buff);
    }
  free(buff);
  return (NULL);
}
